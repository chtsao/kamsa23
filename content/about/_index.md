---
title: "About"
date: 2023-02-08T10:50:42+08:00
draft: false
categories: [adm]
---

-  *Curator*: Kno Tsao. Office: SE A411.  Campus Tel: 3520
-  *Lectures*: Mon 1310-1500, Wed 1310-1400@ SE A323?.
-  Google Classroom: .
-  Office Hours: Mon. 1200-1250; Thr 1510-1600 @ SE A411 or by appointment
-  *Prerequisites:* Statistics, Linear Algebra. Knowledge about regression or General/Generalized Linear Models will be helpful.
-  “Official” computing software: R ([original](http://www.r-project.org/), mirrors @ [NTU](http://cran.csie.ntu.edu.tw/)), [Rstudio](http://www.rstudio.com/)
- References/Texts:
  - Hardle and Simar (2015). Applied Multivariate Statistical Analysis, 4th Edition, Springer. (@[Sprigner](https://link.springer.com/book/10.1007%2F978-3-662-45171-7), [Extras](http://extras.springer.com/2015/978-3-662-45170-0), [Quantlet](http://www.quantlet.de/), [MVA@Github](https://github.com/QuantLet/MVA/), [Webbook](http://sfb649.wiwi.hu-berlin.de/fedc_homepage/xplore/ebooks/html/mva/mvahtml.html), [Data sets](https://www2.karlin.mff.cuni.cz/~hlavka/sms2/index.html)) (Textbook)
  - Johnson and Wichern (2007). Applied Multivariate Statistical Analysis, 6th Edition. Pearson Education International.
  - Hastie, Tibshirani and Friedman (2009). [The Elements of Statistical Learning: Data Mining, Inference and Prediction. 2nd Edition.](https://web.stanford.edu/~hastie/ElemStatLearn/)  (aka. ESLII) Springer-Verlag.
  - Efron and Tibshirani (2016). [Computer Age Statistical Inference. Cambridge](http://web.stanford.edu/~hastie/CASI/).
- References
  - [STAT 505 – Applied Multivariate Statistical Analysis](https://onlinecourses.science.psu.edu/stat505/) @PennState